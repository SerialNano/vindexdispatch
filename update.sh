#!/bin/bash
echo "[Git-Pull] Preparing to pull down latest version"
git reset --hard HEAD
git clean -f
echo "[Git-Pull] Pulling down latest version..."
git pull
echo "[Git-Pull] Local version now matches the most recent commit!"
