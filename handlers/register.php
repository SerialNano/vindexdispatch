<?php

    require('../scripts/logger.php');
    $logger = new DispatchLogger;

    $passwordEntered = $_POST['password'];
    $secondPasswordEntered = $_POST['repeatPassword'];
    $usernameEntered = strtoupper($_POST['username']);

    if($usernameEntered == NULL) {
        header( 'Location: http://172.16.200.29:25361/dispatch/registration.php?status=userblank' );
        die;
    }

    $connection = mysql_connect("localhost","dispatch","vindex")
        or die("Couldn't connect to authentication server.");
    
    $database = mysql_select_db("vindexDispatch", $connection)
        or die("Couldn't gain access to user database.");

    $query2 = "SELECT * FROM users";
    $result2 = mysql_query($query2)
        or die("Query failed with error: " . mysql_error());

    $userExistsInDatabase = FALSE;

    while($row = mysql_fetch_array($result2)) {
        if($row['username'] == $usernameEntered && $row['registered'] == 'YES') {
            mysql_close($connection);
            header( 'Location: http://172.16.200.29:25361/dispatch/registration.php?status=userexists' );
            die;
        } else if ($row['username'] == $usernameEntered && row['registered'] == 'NO') {
            $userExistsInDatabase = TRUE;
        }
    }

    if($passwordEntered == NULL || $passwordEntered == "") {
        mysql_close($connection);
        header( 'Location: http://172.16.200.29:25361/dispatch/registration.php?status=passblank' );
        die;
    }

    if($passwordEntered != $secondPasswordEntered) {
        mysql_close($connection);
        header( 'Location: http://172.16.200.29:25361/dispatch/registration.php?status=passmismatch' );
        die;
    }

    $passwordHash = password_hash($_REQUEST["password"], PASSWORD_BCRYPT);
    $userAddr = $logger->getClientAddr();

    if($userExistsInDatabase) {
        $query = ("UPDATE users SET password='{$passwordHash}',registered='YES',group='PENDING',callsign='UNASSIGNED' WHERE
                    username='{$usernameEntered}'");
    } else {
         $query = ("INSERT INTO users(username, password, registered, group, callsign)
                    VALUES ('{$usernameEntered}','{$passwordHash}', 'YES', 'PENDING', 'UNASSIGNED')");
        $logger->logEvent("REG", "User '{$usernameEntered}' not preconfigured, creating new entry.");
    }

    $result = mysql_query($query)
        or die("Query failed with error: " . mysql_error());

    mysql_close($connection);

    $logger->logEvent("REG", "New user '{$usernameEntered}' ({$userAddr}) registration successful.");
    
    session_start();
    $_SESSION['user'] = $usernameEntered;

    $logger->logEvent("AUTH", "User '{$usernameEntered}' ({$userAddr}) automatically logged in after registration.");

    header( 'Location: http://172.16.200.29:25361/dispatch/dashboard.php' );
    die;
?>
