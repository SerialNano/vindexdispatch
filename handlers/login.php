<?php

    require('../scripts/logger.php');
    $logger = new DispatchLogger;
            
    $passwordEntered = $_REQUEST["password"];
    $usernameEntered = strtoupper($_REQUEST["username"]);
            
    $connection = mysql_connect("localhost","dispatch","vindex")
        or die("Couldn't connect to authentication server.");
    
    $database = mysql_select_db("vindexDispatch", $connection)
        or die("Couldn't gain access to user database.");
    
    $query = "SELECT * FROM users";
    $result = mysql_query($query)
        or die("Query failed with error: " . mysql_error());
            
    while ($row = mysql_fetch_array($result)) {
        if($row['username'] == $usernameEntered && $row['registered'] == 'NO') {
                if(password_verify($passwordEntered, $row['password'])) {
                    mysql_close($connection);
                    
                    $userAddr = $logger->getClientAddr();
                    $logger->logEvent("AUTH", "User '{$usernameEntered}' logged in from {$userAddr}.");
                
                    session_start();
                    $_SESSION['user'] = $usernameEntered;
                    
                    header( 'Location: http://172.16.200.29:25361/dispatch/dashboard.php' );
                    die;
                } else {
                    $userAddr = $logger->getClientAddr();
                    $logger->logEvent("SECURITY", "Failed login attempt for user '{$usernameEntered}' from {$userAddr}.");
                }
        }
    }

    mysql_close($connection);

    header( 'Location: http://172.16.200.29:25361/dispatch/index.php?login=fail' );
    die;      
                        
?>
