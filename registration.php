<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Vindex Dispatch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    </head>
    <body class="w3-dark-gray w3-text-orange">
        <header class="w3-center w3-row">
            <h1><b>Vindex Dispatch</b></h1>
        </header>

        <?php
                $bannerHtml = ('<div class="w3-panel w3-red w3-row w3-round-xlarge w3-margin-left w3-margin-right">
            <span onclick="this.parentElement.style.display=\'none\'" class="w3-closebtn">&times;</span>
            <h3><b>Registration failed</b></h3>
            <p>');

                $failStatus = $_GET['status'];
                if($failStatus == "userblank") {
                    echo("{$bannerHtml}In-game name cannot be blank.</p></div>");
                }
                if($failStatus == "userexists") {
                    echo("{$bannerHtml}User has already been registered!</p></div>");
                }
                if($failStatus == "passblank") {
                    echo("{$bannerHtml}You must enter a password.</p></div>");
                }
                if($failStatus == "passmismatch") {
                    echo("{$bannerHtml}The passwords you entered didn't match.</p></div>");
                }
                if($failStatus == "script") {
                    echo("{$bannerHtml}An internal error has occurred.</p></div>");
                }
        ?>

        <div class="w3-card w3-row w3-margin ">
            <div class="w3-container w3-gray w3-text-white">
                <h3><b>Register for Access</b></h3>
            </div>
            <form class="w3-container w3-white" method="POST" action="handlers/register.php">
                <p>
                    <label class="w3-label w3-text-dark-gray"><b>In-game name</b></label>
                    <input class="w3-input w3-border w3-sand w3-hover-pale-yellow w3-round-large" name="username"
                           type="text">
                </p>
                <p>
                    <label class="w3-label w3-text-dark-gray"><b>Choose a Password</b></label>
                    <input class="w3-input w3-border w3-sand w3-hover-pale-yellow w3-round-large" name="password"
                           type="password">
                </p>
                <p>
                    <label class="w3-label w3-text-dark-gray"><b>Confirm Password</b></label>
                    <input class="w3-input w3-border w3-sand w3-hover-pale-yellow w3-round-large" name="repeatPassword"
                           type="password">
                </p>
                <p>
                    <input class="w3-btn-block w3-gray w3-text-white w3-hover-orange
                                  w3-round-xlarge w3-large" type="submit" value="Register for Access">
                </p>
                <p>
                    <a href="./index.php" class="w3-btn-block w3-gray w3-text-white w3-hover-orange
                                  w3-round-xlarge w3-large">Return to Login</a>
                </p>
            </form>
        </div>
    </body>
</html>
