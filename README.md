# I. Summary #
The **Vindex Dispatch Portal** is a project initiated by **stormhunted** in an attempt to increase the effectiveness of Vindex Dispatch, and to aide emergency response teams in their efforts. By doing so, the rate of successful crimes and the rate of failed rescue attempts are anticipated to decrease.
The system will also allow for quantitative tracking of police/EMT effectiveness, allowing for changes to be evaluated.

# II. Introduction #
While many of you know of me, **stormhunted**, please allow me to briefly introduce myself nonetheless. I am a dispatcher here on Vindex, and as of late, alongside **MAXFANG1** and **TomCubed101**, have been working to clean up and revitalize the Vindex Dispatch as something useful and effective. As granted by **MAXFANG1**, I am currently the impromptu dispatch leader (after him of course), and am always looking for ways to make the department more effective. We've received some criticisms as of late, mostly regarding our actual role, but that's a topic for another day, and in the meantime, we will continue to improve. The project outlined here has been approved by **MAXFANG1**, pending approval of others until later in development when a test release is available, and is an attempt to greatly improve the effectiveness of our Dispatch system.

# III. Problems and Needs #
In this section I will be outlining the needs and problems that this project will strive to remedy.

Problems and Needs of the Vindex Dispatch:

1. Dispatchers have no effective way to pinpoint locations for officers who aren't quite familiar with the map 

2. Despite our efforts, there is currently a lack of coordination between dispatchers when dealing with events

3. The callsign system (work-in-progress) lacks a concrete method of validating callsigns when they're assigned

4. Lackluster reporting by/for emergency response teams or officers

5. Poor officer routing

6. No system for evaluating police or EMT effectiveness

7. Vindex Laws are currently not consolidated, and one must browse extensively to locate obscure laws


These problems prevent emergency services from responding to many emergencies in time. In a three-day study, of 50 emergency calls received, only 44 were actually handled on the dispatch side. Of those 44 handled by dispatchers, only 16 were actually responded to by the services dispatched. Many of the failed responses were due to a lack of communication between departments, or the inability of the dispatched team(s) to find the emergency. By remedying the aforementioned issues, I hope to aide in the reduction of successful crimes and failed rescue attempts.

# IV. Objectives #
The following is a list of goals that will be met by the Vindex Dispatch Portal.

1. Provide an easy-to-understand interface for dispatchers and officers to use to pinpoint locations

2. Improve persisted communication between dispatchers

3. Provide a concrete oversight system for preventing errors in the callsign system

4. Provide an interface for logging, retrieving, and reviewing incident reports by emergency services

5. Provide a framework for tracking metrics regarding police and EMT effectiveness through reporting

6. Compile all laws passed in one place (since the forums Laws section is apparently obsolete)


*Note: The final bullet above regarding laws is the least important and should essentially be disregarded, as it is not a primary objective.*

# V. Evaluation #
The success of this project will be based on the extent of its use post-release and its impact on rate of successful crime and on the rate of successful rescue attempts. A secondary indicator of success will be personnel's likelihood to defer to the system as needed.