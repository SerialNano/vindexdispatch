<?php

    class DispatchLogger {
        function logEvent($type, $details) {
            $logDate = date("Y-m-d");
        
            $handle = fopen("../logs/{$logDate}.log", "ab");
        
            $currentTime = date("Y-m-d H:i:s");
            $prefix = ("[{$currentTime}] ");
        
            if(strlen($type) > 0) {
                $prefix = ("{$prefix}[{$type}] ");
            }
        
            $text = ($prefix . $details . "\n");
           
            if(fwrite($handle, $text) == FALSE) {
                echo("Error: couldn't log to file!");
            }
        
            fclose($handle);
        }
        
        function getClientAddr() {
            $ipaddress = '';
            if ($_SERVER['HTTP_CLIENT_IP'])
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if($_SERVER['HTTP_X_FORWARDED_FOR'])
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if($_SERVER['HTTP_X_FORWARDED'])
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if($_SERVER['HTTP_FORWARDED_FOR'])
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if($_SERVER['HTTP_FORWARDED'])
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if($_SERVER['REMOTE_ADDR'])
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
 
            return $ipaddress;
        }
    }



?>
