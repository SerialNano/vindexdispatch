<?php
    session_start();
    if(isset($_SESSION['user'])) {
        if($_SESSION['user'] != NULL) {
            header( 'Location: http://172.16.200.29:25361/dispatch/dashboard.php' );
            die;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Vindex Dispatch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    </head>
    <body class="w3-dark-gray w3-text-orange">
        <header class="w3-center w3-row">
            <h1><b>Vindex Dispatch</b></h1>
        </header>
         <?php
            $failStatus = $_GET['login'];
            if($failStatus == "fail") {
                echo('<div class="w3-panel w3-red w3-row w3-round-xlarge
                                    w3-margin-left w3-margin-right">
                        <span onclick="this.parentElement.style.display=\'none\'"
                                    class="w3-closebtn">&times;</span>
                        <h3><b>Login failed</b></h3>
                        <p>Invalid username or password.</p>
                    </div>');
            }
        ?>
        <div class="w3-card w3-row w3-margin">
            <div class="w3-container w3-gray w3-text-white">
                <h3><b>Log in</b></h3>
            </div>
            <form class="w3-container w3-white" method="POST" action="handlers/login.php">
                <p>
                    <label class="w3-label w3-text-dark-gray"><b>Username</b></label>
                    <input class="w3-input w3-border w3-sand w3-hover-pale-yellow w3-round-large" name="username"
                           type="text">
                </p>
                <p>
                    <label class="w3-label w3-text-dark-gray"><b>Password</b></label>
                    <input class="w3-input w3-border w3-sand w3-hover-pale-yellow w3-round-large" name="password"
                           type="password">
                </p>
                <p>
                    <input class="w3-btn-block w3-gray w3-text-white w3-hover-orange
                                  w3-round-xlarge w3-large" type="submit" value="Log In">
                </p>
                <p>
                    <a href="./registration.php" class="w3-btn-block w3-gray w3-text-white w3-hover-orange
                                  w3-round-xlarge w3-large">Register for Access</a>
                </p>
            </form>
        </div>
    </body>
</html>
